/**
 * @author Nolan O'Shea
 * 
 * CS 4352
 * Prog 1 Apriori
 *
 */

import java.util.Set;

/**
 * GeneSet is a struct containing a freq gene set and its support
 */
public class GeneSet {
    private Set<String> genes;
    private Double supp;
    
    public GeneSet(Set<String> genes, Double supp) {
        this.genes = genes;
        this.supp = supp;
    }

    public Set<String> getGenes() {
        return genes;
    }

    public Double getSupp() {
        return supp;
    }
}
