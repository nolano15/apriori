/**
 * @author Nolan O'Shea
 * 
 * CS 4352
 * Prog 1 Apriori
 *
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Apriori {
    static final double N = 216,
                        MIN_SUPPORT = .035;
    static final String INPUT = "assignment1_input.txt",
                        OUTPUT = "assignment1_output.txt";
    
    static Map<String, Set<String>> suppmap;
    
    public static void main(String[] args) {
        // suppmap is the file loaded for quick access
        suppmap = new HashMap<String, Set<String>>();
        // current k sized sets being mined
        var cands = new HashSet<Set<String>>();
        
        try(Scanner in = new Scanner(new File(INPUT))) {
            String token, id = null;
            // translate file to HashMap
            while(in.hasNext()) {
                // if token matches id format with dots
                if((token = in.next()).matches(".*\\..*")) {
                    // key becomes selected id
                    id = token;
                    suppmap.put(id, new HashSet<String>());
                } else {
                    // add gene to current id's mapped set
                    suppmap.get(id).add(token);
                    // add gene to set of unique candidates
                    // making a 1 gene set for each token
                    cands.add( Stream.of(token).collect( Collectors.toSet() ) );
                }
            }
        } catch(FileNotFoundException e) {
                e.printStackTrace();
        }
        
        // final results with k as key
        // each sub map has a double which is the support of that gene set
        var result = new HashMap<Integer, Set<GeneSet>>();
        
        int k = 1;
        result.put(k, new HashSet<GeneSet>());
        // drop unsupported 1 gene sets
        suppTest(cands, result.get(k));
        
        while(!result.get(k).isEmpty()) {
            k++;
            
            result.put(k, new HashSet<GeneSet>());
            // need indexes so convert to a list
            var candList = new ArrayList<GeneSet>(result.get(k - 1));
            // refresh cands for new k
            cands = new HashSet<Set<String>>();
            
            // for k <= 2 all subsets will be joined and there will be no pruning
            for(int i = 0; i < candList.size() - 1; i++) {
                for(int j = i + 1; j < candList.size(); j++) {
                    // SELECTIVE JOINING
                    // returns false, and therefore passes the if,
                    // when the two sets share a gene
                    if(k <= 2 || !Collections.disjoint(candList.get(i).getGenes(), candList.get(j).getGenes())) {
                        var prospect = new HashSet<String>(candList.get(i).getGenes());
                        // combine the sets
                        // set behavior will prevent duplicates
                        // prospect contains k genes
                        prospect.addAll(candList.get(j).getGenes());
                        
                        if(k <= 2 || prune(prospect, result.get(k - 1))) {
                            // prospect survived pruning
                            cands.add(prospect);
                        }
                    }
                }
            }
            suppTest(cands, result.get(k));
        }
        
        // Now printing the results
        try(PrintStream out = new PrintStream(new File(OUTPUT))) {
            // last k that had freq gene sets
            k--;
            
            while(k > 1) {
                var gsList = new ArrayList<GeneSet>(result.get(k));
                
                // sort by supports within k gene sets
                Collections.sort(gsList, (gs1,  gs2) -> Double.compare(gs2.getSupp(), gs1.getSupp()));
                
                for(GeneSet gs : gsList) {
                    out.print(gs.getGenes());
                    out.printf(" %.2f", gs.getSupp()*100);
                    out.println("% support");
                }
                
                k--;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Removes gene sets that do not pass the min support
     * 
     * @param cands set of gene sets to test support level
     * @param krow current level being mined
     */
    static void suppTest(Set<Set<String>> cands, Set<GeneSet> krow) {
        for(Set<String> cand : cands) {
            int n = 0;
            // find how many rows have the candidate gene
            for(Set<String> row : suppmap.values()) {
                if(row.containsAll(cand)) {
                    n++;
                }
            }
            // test if frequent
            if(n/N >= MIN_SUPPORT) {
                krow.add(new GeneSet(cand, n/N));
            }
        }
    }
    
    /**
     * Test to determine whether a prospect set should be pruned
     * 
     * @param prospect set being tested for pruning
     * @param krow current row used to check pruning
     * 
     * @return Boolean true if prospect survives, false ow
     */
    static Boolean prune(Set<String> prospect, Set<GeneSet> krow) {
        for(GeneSet gs : krow) {
            // if the prospect contains a k - 1 freq itemset
            // it does not need to be pruned
            if(prospect.containsAll(gs.getGenes())) {
                return true;
            }
        }
        // prospect should be pruned
        return false;
    }
}